# **MentorMate DevCamp: Assignment 2**

## Summary
### Framework: .Net Framework 4.7.2

### Description:
*A Console Application, which creates a solution for a second bricklayer layout, given an input of the initial one.*

### Input

- Wall's dimensions: *width*(rows) and *length*(columns)
- First bricklayer layout: A matrix of values with the stated dimensions (width and length), where each value is separated by a space. 
```
Width: 2
Length: 4

Bricklayer: 
1 1 2 2
3 3 4 4

```

### Output
- Second bricklayer layout: A possible layout of the second bricklayer. Each brick is surrounded by a "*" frame.
```
Bricklayer: 

*********
*1*2 2*4*
* ***** *
*1*3 3*4*
*********

```
