﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MentorMate_Brickwork
{
    /*Summary: The class BrickworkException inherits the class Exception and represents errors that occur during application execution.
   */
    [Serializable]
    public class BrickworkException:Exception
    {
        public BrickworkException (string message) : base(message) { }
    }
}
