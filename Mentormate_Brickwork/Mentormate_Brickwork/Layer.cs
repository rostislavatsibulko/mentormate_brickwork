﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace MentorMate_Brickwork
{
    /*Summary: The class Layer represents a base class, that contains the needed data for a single brick layer and the functional logic behind the object.
      Declared properties:
            - dimensions of the wall and each of the wall's layers;
            - position of each brick;
            - total numbers of bricks in a layer;

    */
    public class Layer
    {
        private int _width;
        private int _length;
        private int _totalBricks;
        private int[,] _layout;

       public int Width
        {
            get { return _width; }
            set {
                if (!ValidateLayerDimensions(value))
                {
                    throw new BrickworkException(ExceptionMessegesList.INVALID_LAYER_WIDTH);
                }
                else _width = value;
            }
        }

        public int Length
        {
            get { return _length; }
            set
            {
                if (!ValidateLayerDimensions(value))
                {
                    throw new BrickworkException(ExceptionMessegesList.INVALID_LAYER_LENGTH);
                }
                else _length = value;
            }
        }

        public int TotalBricks
        {
            get { return _totalBricks; }
            set { _totalBricks = value; }
        }

        public int[,] Layout
        {
            get { return _layout; }
            set { _layout = value; }
        }

        public Layer(int width, int length)
        {
            this.Width = width;
            this.Length = length;                      
            this.TotalBricks = (width * length) / 2;
            this.Layout = new int[width, length];            
        }

        /*Purpose: Validates the input dimensions. The imput must be an even number, between 0 and 100.
          Returns: Boolean. Returns false, if any of the requirements are not met.
        */
        private bool ValidateLayerDimensions(int dimension)
        {
            if ((dimension % 2 != 0) || (dimension < 0) || (dimension > 100))
            {
                return false;
            }
            else return true;
        }

        /*Purpose: Handles the input of the bricks' position of the initial layer.
          Returns: Void.
        */
        public void InputBrickLayer()
        {
            int bricksCounter = 0;

            Console.WriteLine("Please enter a valid bricks input:");

            for (int i = 0; i < this.Width; i++)
            {
                int[] temp = Console.ReadLine().Split(' ').Select(int.Parse).ToArray();
                for (int k = 0; k < this.Length; k++)
                {
                    while(temp.Length != this.Length) 
                    {
                        temp = Console.ReadLine().Split(' ').Select(int.Parse).ToArray();
                    }
                    this.Layout[i, k] = temp[k];
                    bricksCounter++;
                }
            }
                   
            bricksCounter = bricksCounter / 2;

            if (this.IsBrickSpanning3RowsOrColums())
            {
                throw new BrickworkException(ExceptionMessegesList.INVALID_BRICK_SIZE);
            }
            else if (!this.ValidateNumberOfBricks(bricksCounter))             
            {
                throw new BrickworkException(ExceptionMessegesList.INVALID_BRICKLAYER);
            }
           

            Console.WriteLine("The first layer of bricks is successfully created.");
        }


        /*Purpose: Validates if the input of the bricks' positions, suits the set dimensions of the wall.
          Returns: Boolean. Returns false, if any of the requirements are not met.
        */
        private bool ValidateNumberOfBricks(int bricksCounted)
        {
            if ((bricksCounted != this.TotalBricks) || (this.Layout.GetLength(0) != this.Width) || (this.Layout.GetLength(1) != this.Length))
            {
                return false;
            }
            else return true;                
        }

        /*Purpose: Validates if a single or more bricks are spanning over 3 positions.
          Returns: Boolean. Returns false, if the requirement is not met.
        */
        private bool IsBrickSpanning3RowsOrColums()
        {
           for (int i = 0; i < this.Width - 2; i++)
           {
                    for (int k = 0; k < this.Length - 2; k++)
                    {
                        if (this.Layout[i, k] == (this.Layout[i + 2, k]))
                        {
                            return false;
                        }
                        if (this.Layout[i, k] == (this.Layout[i, k + 2]))
                        {
                            return false;
                        }
                    }
           }
           return true;
        }

        /*Purpose: Generates a suitable layout of the next layer in accordance with the requirement.
          Returns: Boolean. Returns false, if no solution exists.
        */
        public bool CreateBrickLayer(int[,] currentLayerLayout, int brickNumber)
        {
            
            if (brickNumber > this.TotalBricks)
            {
                return true;
            }
            else 
            {
                for (int i = 0; i < this.Width; i++)
                {
                    for (int k = 0; k < this.Length; k++)
                    {
                        if (currentLayerLayout[i, k] == 0)
                        {
                            if (IsMoveToTheRightAllowed(currentLayerLayout, i, k))
                            {
                                currentLayerLayout[i, k] = currentLayerLayout[i, k + 1] = brickNumber;
                                
                                if (CreateBrickLayer(currentLayerLayout, brickNumber + 1))
                                {
                                    return true;
                                }
                                else
                                {
                                    currentLayerLayout[i, k] = currentLayerLayout[i, k + 1] = 0;
                                }
                            }
                            else if (IsMoveDownwardsAllowed(currentLayerLayout, i, k))
                            {
                                currentLayerLayout[i, k] = currentLayerLayout[i + 1, k] = brickNumber;
                              
                                if (CreateBrickLayer(currentLayerLayout, brickNumber + 1))
                                {
                                    return true;
                                }
                                else
                                {
                                    currentLayerLayout[i, k] = currentLayerLayout[i + 1, k] = 0;                                  
                                }
                            }
                        }
                    }
                }
                return false;
            }
            
        }

        /*Purpose: Checks if there is a possibility a brick to be moved downwards with one cell.
         Returns: Boolean. Returns false, if there is no next row, the cell is already taken or the brick is lying on the current and the next cell.
        */
        private bool IsMoveDownwardsAllowed(int[,] currentLayerLayout, int cellRow, int cellColumn)
        {
            if ((cellRow + 1 >= this.Width) || (currentLayerLayout[cellRow + 1, cellColumn] != 0) || (this.Layout[cellRow, cellColumn] == this.Layout[cellRow + 1, cellColumn]))
            {
                return false;
            }
            else return true;
        }

        /*Purpose: Checks if there is a possibility a brick to be moved to the right with one cell.
         Returns: Boolean. Returns false, if there is no next column, the cell is already taken or the brick is lying on the current and the next cell.
        */
        private bool IsMoveToTheRightAllowed(int[,] currentLayerLayout, int cellRow, int cellColumn)
        {
            if ((cellColumn + 1 >= this.Length) || (currentLayerLayout[cellRow, cellColumn + 1] != 0) || (this.Layout[cellRow, cellColumn] == this.Layout[cellRow, cellColumn + 1]))
            {
                return false;
            }
            else return true;
        }

        /*Purpose: Prints the final new layer in the console. Each brick is surrounded by asterisk symbols.
        Returns: Void.
       */
        public void OutputBrickLayer(Layer brickLayer)
        {
            string[,] outputBrickLayer = new string[(brickLayer.Width * 2) + 1, (brickLayer.Length * 2) + 1];

            OutputTopRow(outputBrickLayer);
            OutputFirstColumn(outputBrickLayer);
            
            for (int i = 0; i < brickLayer.Width - 1; i++)
            {
                for (int k = 0; k < brickLayer.Length - 1; k++)
                {
                    if (brickLayer.Layout[i, k] == brickLayer.Layout[i + 1, k])
                    {
                        OutputVerticalFrame(brickLayer, outputBrickLayer, i, k);                       
                    }

                    if (brickLayer.Layout[i, k] == brickLayer.Layout[i, k + 1])
                    {
                        OutputHorizontalFrame(brickLayer, outputBrickLayer, i, k);                       
                    }
                }
            }

            for (int i = 0; i < brickLayer.Width - 1; i++)
            {
                int k = brickLayer.Length - 1;
                if (brickLayer.Layout[i, k] == brickLayer.Layout[i + 1, k])
                {
                    OutputVerticalFrame(brickLayer, outputBrickLayer, i, k);
                }
            }

            for (int k = 0; k < brickLayer.Length - 1; k++)
            {
                int i = brickLayer.Width - 1;
                if (brickLayer.Layout[i, k] == brickLayer.Layout[i, k + 1])
                {
                    OutputHorizontalFrame(brickLayer,outputBrickLayer,i,k);                   
                }
            }

            Console.WriteLine("The second layer of bricks is successfully created, in accordance with the requirements set out:\n");

            for (int i = 0; i < outputBrickLayer.GetLength(0); i++)
            {
                for (int k = 0; k < outputBrickLayer.GetLength(1); k++)
                {
                    Console.Write(outputBrickLayer[i, k]);
                }
                Console.WriteLine();
            }
        }

        /*Purpose: Generates the first row of the asterisk frame.
          Example: 
          2x4 *********
        */
        private void OutputTopRow(string[,] outputBrickLayer)
        {
            for (int i = 0; i < outputBrickLayer.GetLength(1); i++)
            {
                outputBrickLayer[0, i] = "*";
            }
        }

        /*Purpose: Generates the first column of the asterisk frame.
          Example: 
          2x4 *********
              *
              * 
              * 
              * 
        */
        private void OutputFirstColumn(string[,] outputBrickLayer)
        {
            for (int i = 0; i < outputBrickLayer.GetLength(0); i++)
            {
                outputBrickLayer[i, 0] = "*";
            }
        }

        /*Purpose: Generates the half of asterisks' frame of a vertical brick.
          Example: 
          2x4 *********
              *1*
              * *
              *1*
              ***
        */
        private void OutputVerticalFrame(Layer brickLayer, string[,] outputBrickLayer, int i, int k)
        {
            outputBrickLayer[i * 2 + 1, k * 2 + 1] = brickLayer.Layout[i, k].ToString(); 
            outputBrickLayer[i * 2 + 3, k * 2 + 1] = brickLayer.Layout[i, k].ToString();
            outputBrickLayer[(i * 2 + 2), (k * 2 + 1)] = " ";
            outputBrickLayer[(i * 2 + 4), (k * 2 + 1)] = "*";
            outputBrickLayer[(i * 2 + 1), (k * 2 + 2)] = "*";
            outputBrickLayer[(i * 2 + 2), (k * 2 + 2)] = "*";
            outputBrickLayer[(i * 2 + 3), (k * 2 + 2)] = "*";
            outputBrickLayer[(i * 2 + 4), (k * 2 + 2)] = "*";
        }

        /*Purpose: Generates the half of asterisks' frame of a horizontal brick.
          Example: 
          2x4 *********
              *1*2 2*
              * *****
              *1*
              ***
       */
        private void OutputHorizontalFrame(Layer brickLayer, string[,] outputBrickLayer, int i, int k)
        {
            outputBrickLayer[i * 2 + 1, k * 2 + 1] = brickLayer.Layout[i, k].ToString(); 
            outputBrickLayer[i * 2 + 1, k * 2 + 3] = brickLayer.Layout[i, k].ToString();
            outputBrickLayer[(i * 2 + 1), (k * 2 + 4)] = "*";
            outputBrickLayer[(i * 2 + 1), (k * 2 + 2)] = " ";
            outputBrickLayer[(i * 2 + 2), (k * 2 + 1)] = "*";
            outputBrickLayer[(i * 2 + 2), (k * 2 + 2)] = "*";
            outputBrickLayer[(i * 2 + 2), (k * 2 + 3)] = "*";
            outputBrickLayer[(i * 2 + 2), (k * 2 + 4)] = "*";
        }
    }
}

