﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MentorMate_Brickwork
{
    /*Summary: The class ExceptionMessegesList contains the messeges of all exeptions, that occur during application execution.
    */
    class ExceptionMessegesList
    {
            public const string INVALID_LAYER_WIDTH = "Please note that the number of rows (a.k.a wall width) must be an even number between 0 and 100.";
            public const string INVALID_LAYER_LENGTH = "Please note that the number of columns (a.k.a wall length) must be even number between 0 and 100.";
            public const string INVALID_BRICK_SIZE = "Please note that a brick cannot be spanning three and more rows or columns.";
            public const string INVALID_BRICKLAYER = "Be informed that the dimensions of the wall do not match the bricks input.";
            public const string INVALID_SOLUTION = "-1/n No solution was found.";


    }
}
