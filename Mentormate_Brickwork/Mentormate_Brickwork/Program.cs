﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MentorMate_Brickwork
{
    /*Summary: The class Program contains two methods:
                   - Main method;
                   - InputLayerDimensions();
    */
    class Program
    {
        /*Purpose: Entry point of the application. Declares the first and the second layers of the wall.
                   Calls a method on the first layer in order to initialize it.
                   Calls a method on the second layer in order to generate a proper bricks' layout.
                   Calls a method on the second layer in order to print in a console the final visualization of the new layer.
        */
        static void Main(string[] args)
        {
            Layer firstBrickLayer = InputLayerDimensions();
            Layer secondBrickLayer = new Layer(firstBrickLayer.Width, firstBrickLayer.Length);

            firstBrickLayer.InputBrickLayer();
            firstBrickLayer.CreateBrickLayer(secondBrickLayer.Layout, 1);                      
            secondBrickLayer.OutputBrickLayer(secondBrickLayer);                     
        }

        /*Purpose: Requests the wall dimensions and creates the first layer.
         Returns: Layer. Returns the created new instance of a layer with set dimensions.
       */
        public static Layer InputLayerDimensions()
        {
            Console.WriteLine("Please enter valid wall dimensions (<Width> <Lenght>):");

            int[] input = Console.ReadLine().Split(' ').Select(int.Parse).ToArray();
            Layer newLayer = new Layer(input[0], input[1]);

            Console.WriteLine("The dimensions of the wall are successfully set.");

            return newLayer;
        }
    
    }
}
